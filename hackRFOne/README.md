﻿hackRFOne
===================

Contains gnuradio-companion files usable with HackRFOne.

> <i class="icon-file"></i> **Files and Folders**
> - **testRadio.grc** Contains blocks for listening FM Radio (usefull for testing HackRFOne). 
> - **channelModeling/** Contain files used for the over-land channel caracterisation. 
