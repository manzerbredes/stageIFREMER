﻿stageIFREMER
==================

#### <i class="icon-folder"></i> Folders structure

 - **GEOLOC** :  It contains source code for turtle and gateways experiments on Raspberry Pi 3 using Dragino Shield.
 - **R** : It contains R code used during the internship.
 - **hackRFOne** : It contains files used with the HackRFOne and GNURadio tools.
