R Files
===================

Some explanations about the R files.

-----

#### <i class="icon-file"></i> intersectionCercles.R

Find the intersection points of two circles.

#### <i class="icon-file"></i> multilateration.R

Do multilateration using multiple gateways.

#### <i class="icon-file"></i> sx1276.R

Find sx1276 time on air.

#### <i class="icon-file"></i> tools.R

Some functions.

#### <i class="icon-file"></i> loadDataExp.R

Do multilateration on GEOLOC saved data (gps.csv and records folder).
