# Changer les paramètres plus bas et éxécuter ce fichier afin de calculer la durée
# d'émission d'une trame en Lora sur le module sx1276
# Voir sx1276 datasheet pour le détails de ces paramètres :
PL=10  
SF=12
IH=0
DE=0
CR=1
CRC=0
BW=500*10^3
nPreamble=6

# Cf SX1276 datasheet
getNPayload=function(PL,SF,IH,DE,CR,CRC){
  numerateur=8*PL-4*SF+28+16*CRC-20*IH;
  denominateur=4*(SF-2*DE);
  ceil=ceiling(numerateur/denominateur);
  np=(8+max(ceil*(CR+4),0));
  return(np);
}

# Cf SX1276 datasheet
getTPrembule=function(nPreamble,Rs){
  return((nPreamble+4.25)*(1/Rs));
}

# Cf SX1276 datasheet
getTPayload=function(nPayload,Rs){
  return(nPayload*(1/Rs));  
}

# Cf SX1276 datasheet
getRs=function(BW,SF){
  Rs=BW/(2^SF)
  return(Rs);
}

getTPacket=function(PL,SF,IH,DE,CR,CRC,BW,nPreamble){
  Rs=getRs(BW,SF);
  print(paste("Symbol duration",Rs,"s"))
  tPrembule=getTPrembule(nPreamble,Rs);
  print(paste("Preambule duration",tPrembule,"s"))
  nPayload=getNPayload(PL,SF,IH,DE,CR,CRC);
  tPayload=getTPayload(nPayload,Rs);
  print(paste("Payload duration",tPayload,"s"))
  return(tPrembule+tPayload);
}

# Afficher le calcule
print(paste("Packet duration",getTPacket(PL,SF,IH,DE,CR,CRC,BW,nPreamble),"s"))