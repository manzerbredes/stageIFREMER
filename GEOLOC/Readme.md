GEOLOC
===================


Some explanations about the GEOLOC project.

----------


Folders structure
-------------

#### <i class="icon-folder"></i> lib

Contains library for accessing to dragino shield components (GPS and SX1276). It contains also tools for SX1276 registers configurations.


#### <i class="icon-folder"></i> app

Contains mobile application (turtle transmitter) and anchor application (gateway).


#### <i class="icon-folder"></i> app/anchor/socket

Contains application for gateway communication.

#### <i class="icon-folder"></i> records

Folder created by the GEOLOC application to store received frame from mobile application and slave gateways.

Build the project
-------------

Configure the project by changing variable from <i>Makefile</i> :

1. **TARGET** :  Choose between MOBILE or ANCHOR (take care to do not add space at the end of the variable)
2. **ANCHOR_ID** : Change the anchor id
3. **IS_MASTER** : Define if the anchor is the master 1 for yes and 0 for no

Next run the next command in the project root folder to build the project :

    make
 To clear the project run :
 

    make clean
 