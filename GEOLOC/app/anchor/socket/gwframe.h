#ifndef gwframe_h
#define gwframe_h

#include "../../frame.h"
#include "../../../lib/gps.h"

typedef struct GWFrame GWFrame;
struct GWFrame {
	short slaveID;
	struct NMEA_GGA ggaFrame;
	int rssi;
	Frame frame;
};


#endif
