#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include "com.h"
#include "../records.h"

static void *rcvIPFromMasterThread(void *args){
	while(1){
		rcvIPFromMaster();
		puts("Master IP receive !");
		//delay(IP_SEND_INTERVAL/2); // Add delay to free cpu resources
	}
}

static void *sendIPToSlaveThread(void *args){
	while(1){
		puts("Send Master IP to slave...");
		sendIPToSlave();
		delay(IP_SEND_INTERVAL); // Wait five minutes
	}
}


static void *rcvDataFromSlaveThread(void *args){
	while(1){
		GWFrame frame;
		frame=rcvDataFromSlave();
		puts("Frame receive from a slave !");
		saveFrame(frame);	
	}
}

void startGWCom(){

#if IS_MASTER != 0
		pthread_t sendIPThread, rcvDataThread;
		pthread_create(&sendIPThread, NULL, sendIPToSlaveThread, NULL);
		pthread_create(&rcvDataThread, NULL, rcvDataFromSlaveThread, NULL);
#else
		pthread_t rcvIPThread;
		pthread_create(&rcvIPThread, NULL, rcvIPFromMasterThread, NULL);
#endif

}
