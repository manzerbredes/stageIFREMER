#include<stdio.h>
#include<string.h>    //strlen
#include<sys/socket.h>
#include <unistd.h>
#include<arpa/inet.h> //inet_addr
#include "com.h"




void rcvIPFromMaster()
{
	int socketID;
	struct sockaddr_in SockAddr;
	int allowBC=1;

	socketID=socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
	setsockopt(socketID,SOL_SOCKET,SO_BROADCAST,(void*) &allowBC,sizeof(allowBC));

	SockAddr.sin_family=AF_INET;
	SockAddr.sin_port=htons(DATA_PORT);
	SockAddr.sin_addr.s_addr=htonl(INADDR_BROADCAST);


	int binded=bind(socketID,(struct sockaddr *) &SockAddr,sizeof(SockAddr));
	if(binded<0){
		puts("Failed to bind socket for receiving IP from master.");
		close(socketID);
	}
	else{
		listen(socketID,3);
		socklen_t src_addr_len=sizeof(SockAddr);
		char buffer[MASTER_IP_SIZE];
		memset(buffer, 0x0,MASTER_IP_SIZE);
		recvfrom(socketID,buffer,sizeof(buffer),0,(struct sockaddr*)&SockAddr,&src_addr_len);
		close(socketID);
		puts(buffer);

		pthread_mutex_lock(&mutex_master_ip);
		memset(MASTER_IP, 0x0,MASTER_IP_SIZE);
		memcpy(MASTER_IP, buffer,MASTER_IP_SIZE);
		pthread_mutex_unlock(&mutex_master_ip);
	}

}
