
#include<stdio.h>
#include<string.h>    //strlen
#include<sys/socket.h>
#include <unistd.h>
#include<arpa/inet.h> //inet_addr
#include "com.h"
#include "./gwframe.h"
#include "../records.h"


int serverTCP_socket_desc=-1;

GWFrame rcvDataFromSlave(){

	if(serverTCP_socket_desc==-1){
		// Create socket
		serverTCP_socket_desc=socket(AF_INET, SOCK_STREAM,0);
		if(serverTCP_socket_desc==-1){
			puts("Failed to create socket");
			exit(1);
		}
		struct sockaddr_in listen_addr;

		listen_addr.sin_family = AF_INET;
		listen_addr.sin_addr.s_addr = INADDR_ANY;
		listen_addr.sin_port = htons(DATA_PORT);

		int binded=bind(serverTCP_socket_desc,(struct sockaddr *) &listen_addr,sizeof(listen_addr));
		if(binded<0){
			puts("Failed to bind, trying until it work...");
			while(binded<0){
				binded=bind(serverTCP_socket_desc,(struct sockaddr *) &listen_addr,sizeof(listen_addr));
				sleep(1);
			}
			puts("Bind succeed !");
		}

		listen(serverTCP_socket_desc,3);
	}

	int client,len;
	struct sockaddr_in client_addr;
	client=accept(serverTCP_socket_desc, (struct sockaddr *)&client_addr, (socklen_t*)&len);
	if(client<0){
		close(serverTCP_socket_desc);
		serverTCP_socket_desc=-1;
		puts("Acceptation failed");
	}
	else{
		GWFrame frame;
		recv(client, &frame, sizeof(GWFrame),0);
		char msg[100]="Données bien reçus par la gateway maitre.";
		write(client, msg,strlen(msg));
		close(client);
		return(frame);

	}
}


