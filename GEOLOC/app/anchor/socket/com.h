#ifndef com_h
#define com_h

#include "gwframe.h"

#define IP_SEND_INTERVAL 300000
#define DATA_PORT 8888
#define IFACE "wlan0"
#define MASTER_IP_SIZE 100 // Buffer size for IP address char

char MASTER_IP[MASTER_IP_SIZE]; // Master IP (global variable)
pthread_mutex_t mutex_master_ip; // Mutex for thread that use MASTER_IP

/**
 * Init GW communication
 */
void startGWCom();

/**
 * Send GW frame to master GW
 */
void sendDataToMaster(GWFrame frame);

/**
 * Start master GW slave frame receiver server
 */
GWFrame rcvDataFromSlave();

/**
 * Start slave ip reveiver server
 */
void rcvIPFromMaster();

/**
 * Send master ip to slaves (broadcast UDP)
 */
void sendIPToSlave();


#endif
