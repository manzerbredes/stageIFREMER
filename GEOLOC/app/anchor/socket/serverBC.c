#include<stdio.h>
#include<string.h>    //strlen
#include<sys/socket.h>
#include <net/if.h>
#include "com.h"
#include <unistd.h>
#include <sys/ioctl.h>
#include<arpa/inet.h> //inet_addr



static char *getIp(){
	int fd;
	struct ifreq ifr;

	char iface[] = IFACE;

	fd = socket(AF_INET, SOCK_DGRAM, 0);

	//Type of address to retrieve - IPv4 IP address
	ifr.ifr_addr.sa_family = AF_INET;

	//Copy the interface name in the ifreq structure
	strncpy(ifr.ifr_name , iface , IFNAMSIZ-1);

	ioctl(fd, SIOCGIFADDR, &ifr);

	close(fd);
	return(inet_ntoa(( (struct sockaddr_in *)&ifr.ifr_addr )->sin_addr));
}

void sendIPToSlave()
{
	int socketID;
	struct sockaddr_in SockAddr;
	int allowBC=1;

	socketID=socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
	setsockopt(socketID,SOL_SOCKET,SO_BROADCAST,(void*) &allowBC,sizeof(allowBC));

	SockAddr.sin_family=AF_INET;
	SockAddr.sin_port=htons(DATA_PORT);
	SockAddr.sin_addr.s_addr=htonl(INADDR_BROADCAST);

	char *addr;
	addr=getIp();
	sendto(socketID,addr,strlen(addr),0,(struct sockaddr*)&SockAddr, sizeof(SockAddr));

}


