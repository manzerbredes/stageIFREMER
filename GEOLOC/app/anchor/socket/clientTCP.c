#include<stdio.h>
#include<string.h>    //strlen
#include <stdlib.h>
#include<sys/socket.h>
#include <unistd.h>
#include<arpa/inet.h> //inet_addr
#include "com.h"

void sendDataToMaster(GWFrame frame){

	int socket_desc;
	struct sockaddr_in server;
	socket_desc=socket(AF_INET, SOCK_STREAM,0);
	char message[1000]="GET / HTTP/1.1\r\n\r\n";
	char serverReply[2000];

	if(socket_desc==-1){
		puts("Failed to create socket.");
	}
	else{
		// Configure server
		pthread_mutex_lock(&mutex_master_ip);
		server.sin_addr.s_addr = inet_addr(MASTER_IP);
		pthread_mutex_unlock(&mutex_master_ip);
		server.sin_family = AF_INET;
		server.sin_port = htons(DATA_PORT);

		if(connect(socket_desc,(struct sockaddr *)&server, sizeof(server))<0){
			puts("Failed to connect to server");
		}
		else{
			puts("Socket connected");
			if(send(socket_desc, &frame, sizeof(GWFrame),0)<0){
				puts("Failed to send message.");
			}
			puts("Data send !");
			if(recv(socket_desc, serverReply,2000,0)<0){
				puts("Timeout");
			}
			puts(serverReply);

		}

		close(socket_desc);
	}
}
