#include <stdio.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <sys/types.h>
#include <string.h>
#include "app.h"
#include "../frame.h"
#include <pthread.h>
#include "../../lib/gps.h"
#include "socket/com.h"
#include "records.h"

//extern struct NMEA_GGA NmeaGgaFrame;
pthread_mutex_t mutex_NmeaGgaFrame; // Mutex for thread that use MASTER_IP

void runApp(Config config){
	// Hello msg
	printf("\n|Starting gateway application|\n\n");

	// Start GW communication
	startGWCom();

	// Ensure we are in standby mode and apply configuration
	config.mode=MODE_STDBY;
	applyMode(config);
	applyConfig(config);

	config.mode=MODE_RX;
	applyMode(config);

	pthread_t frameHandlerThread;

	printf("Wait for packet...\n");                                                                                                         
	while(1){                                                                                                                                 
		if(digitalRead(0x7)==1){                                                                                                          
			// Build parameters
			Frame frame=pullFrame();
			int rssi=fetchRSSI();
			void *param;
			param=malloc(sizeof(Frame)+sizeof(int));
			*((Frame *)param)=frame;
			*((int *)(param+sizeof(Frame)))=rssi;
			// Run thread
			pthread_create(&frameHandlerThread, NULL, handleMessage, param);
			config.mode=MODE_STDBY;                                                                                                   
			applyMode(config);                                                                                                        
			config.mode=MODE_RX;
			applyMode(config);
		}                                                                                                                                 
	} 
	pthread_join(frameHandlerThread,NULL);

}

void *handleMessage(void *args){
	// Fetch parameters
	Frame frame=*((Frame *)args);
	int rssi=*((int *)(args+sizeof(Frame)));

	// Print informations
	printf("Packet receive !!!\n\n");                                                                                         
	printf("Frame info :\nId : %d\nStamp : %d\nData 1-6 : %d %d %d %d %d %d\nRSSI : %d\n\n",
			frame.id, frame.stamp,
			frame.data[0],frame.data[1],frame.data[2],frame.data[3],frame.data[4],frame.data[5],
			rssi);

	GWFrame masterFrame;
	masterFrame.slaveID=ANCHOR_ID;

	pthread_mutex_lock(& mutex_NmeaGgaFrame);
	masterFrame.ggaFrame=getNMEA_GGAFrame();
	pthread_mutex_unlock(& mutex_NmeaGgaFrame);

	masterFrame.frame=frame;
	masterFrame.rssi=rssi;

	if(!IS_MASTER){
		sendDataToMaster(masterFrame);
	}
	else{
		saveFrame(masterFrame);
	}
}
