#ifndef app_h
#define app_h

#include "../frame.h"
#include "../../lib/config.h"

/**
 * Run the application
 */
void runApp(Config config);

/**
 * Used to handle received frame from mobile
 */
void *handleMessage(void *args);

#endif
