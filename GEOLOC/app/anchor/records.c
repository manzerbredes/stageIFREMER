
#include "records.h"
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

void saveFrame(GWFrame frame){
	// Create records dir if not exists
	mkdir(RECORDS_DIR,0777);

//	// Build string
	char idStr[50];                                                                                                                        
	sprintf(idStr,"/anchor_%d", frame.slaveID);                                                                                                      
	char filePath[200];
	filePath[0]='\0';
	strcat(filePath, RECORDS_DIR);
	strcat(filePath, idStr);

//	// Save in file
	FILE *file;
	short writeHeader=0;
	pthread_mutex_lock (& mutex_file);
	if(access( filePath, F_OK ) == -1){
		writeHeader=1;
	}
	file=fopen(filePath,"a+");
	if(file!=NULL){
		if(writeHeader){
			fprintf(file,"GWID,GPSSTATE,LatDeg,LatMin,LatSec,LatDir,LonDeg,LonMin,LonSec,LonDir,turtleID,rssi\n");
		}
		fprintf(file,"%d,%d,%d,%d,%f,%d,%d,%d,%f,%d,%d,%d\n",
				frame.slaveID,
				frame.ggaFrame.state,
				frame.ggaFrame.latDeg,
				frame.ggaFrame.latMin,
				frame.ggaFrame.latSec,
				frame.ggaFrame.latDir,
				frame.ggaFrame.lonDeg,
				frame.ggaFrame.lonMin,
				frame.ggaFrame.lonSec,
				frame.ggaFrame.lonDir,
				frame.frame.id,
				frame.rssi);
		fclose(file);
	}
	else{
		printf("Failed to open file %s.\n",filePath);
	}
	pthread_mutex_unlock (& mutex_file);
}
