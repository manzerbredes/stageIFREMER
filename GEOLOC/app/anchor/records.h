#ifndef records_h
#define records_h

#include <pthread.h>
#include "socket/gwframe.h"

#define RECORDS_DIR "./records"

// Mutex for thread writing in same file
pthread_mutex_t mutex_file;

/**
 * Save GWFrame into file
 */
void saveFrame(GWFrame frame);

#endif
